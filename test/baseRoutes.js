const expect  = require('chai').expect;
const request = require('request');

const server = require('../server.js');

describe('HAPI BaseRoutes', async () => {
/*
  before(async () => {
    await server.start();
  }); */

  it('GET /', (done) => {
    request('http://localhost:8245', function(error, response, body) {
      expect(response.statusCode).to.equal(200);
      expect(JSON.parse(body)).to.be.an('object');
      done();
    });
  });

  after(async () => {
    await server.stop();
  });
});
