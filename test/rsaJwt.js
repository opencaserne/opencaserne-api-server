var expect  = require('chai').expect;

const rsaJwt = require('../src/utils/rsaJwt')();
const UUID = require('uuid/v4');

describe('rsaJwt', () => {
  it('Configuration', (done) => {
    expect(rsaJwt.ready()).to.equal(true);
    done();
  });

  describe('Usage', () => {

    const testPayload = { userUuid: 'test-' + UUID(), testObj: 'testValue' };
    const secondTestPayload = { userUuid: 'test-' + UUID(), testObj: 'testValue2' };

    let token, secondToken;
    let beforeSign, afterSign;

    it('Sign', (done) => {
      beforeSign = Math.floor(new Date().getTime()/1000);
      token = rsaJwt.sign(testPayload);
      afterSign = Math.floor(new Date().getTime()/1000);
      secondToken = rsaJwt.sign(secondTestPayload);

      expect(typeof token).to.equal('string');
      done();
    });

    it('Verify', (done) => {
      const decodedPayload = rsaJwt.verify(token);

      expect(decodedPayload).to.be.an('object');
      expect(rsaJwt.verify(secondToken)).to.be.an('object');
      expect(decodedPayload.iat >= beforeSign).to.be.true;
      expect(decodedPayload.iat <= afterSign).to.be.true;
      expect(testPayload.userUuid).to.equal(decodedPayload.userUuid);
      expect(testPayload.testObj).to.equal(decodedPayload.testObj);
      done();
    });

    it('Invalidate', (done) => {
      rsaJwt.invalidateOldUserToken(testPayload.userUuid);

      expect(rsaJwt.verify(token)).to.be.false;
      expect(rsaJwt.verify(secondToken)).to.be.an('object');
      done();
    });

    it('Sign after invalidate', (done) => {
      beforeSign = Math.floor(new Date().getTime()/1000);
      token = rsaJwt.sign(testPayload);
      afterSign = Math.floor(new Date().getTime()/1000);

      expect(typeof token).to.equal('string');
      done();
    });

    it('Verify after invalidate', (done) => {
      const decodedPayload = rsaJwt.verify(token);

      expect(decodedPayload).to.be.an('object');
      expect(rsaJwt.verify(secondToken)).to.be.an('object');
      expect(decodedPayload.iat >= beforeSign).to.be.true;
      expect(decodedPayload.iat <= afterSign).to.be.true;
      expect(testPayload.userUuid).to.equal(decodedPayload.userUuid);
      expect(testPayload.testObj).to.equal(decodedPayload.testObj);
      done();
    });

    it('Generate new key pair', (done) => {
      rsaJwt.generateNewKeyPair();

      expect(rsaJwt.ready()).to.equal(true);
      expect(rsaJwt.verify(token)).to.be.false;
      expect(rsaJwt.verify(secondToken)).to.be.false;
      done();
    });

    it('Sign after new key pair', (done) => {
      beforeSign = Math.floor(new Date().getTime()/1000);
      token = rsaJwt.sign(testPayload);
      afterSign = Math.floor(new Date().getTime()/1000);

      expect(typeof token).to.equal('string');
      done();
    });

    it('Verify after new key pair', (done) => {
      const decodedPayload = rsaJwt.verify(token);

      expect(decodedPayload).to.be.an('object');
      expect(decodedPayload.iat >= beforeSign).to.be.true;
      expect(decodedPayload.iat <= afterSign).to.be.true;
      expect(testPayload.userUuid).to.equal(decodedPayload.userUuid);
      expect(testPayload.testObj).to.equal(decodedPayload.testObj);
      done();
    });
  });
});
