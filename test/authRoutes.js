const expect  = require('chai').expect;
const request = require('request');

const server = require('../server.js');

describe('HAPI AuthRoutes', async () => {

  before(async () => {
    await server.start();
  });

  it('GET /auth/session', (done) => {
    request('http://localhost:8245/auth/session', {method: 'post'}, (error, response, body) => {
      expect(response.statusCode).to.equal(400);
      expect(JSON.parse(body)).to.be.an('object');
      done();
    });
  });

  it('GET /auth/session', (done) => {
    request.post('http://localhost:8245/auth/session', {method: 'post', json: {emailOrUsername: 'openCaserne', password: 'test'}}, (error, response, body) => {
      expect(response.statusCode).to.equal(200);
      expect(body).to.be.an('object');
      done();
    });
  });

/*
  after(async () => {
    await server.stop();
  }); */
});
