'use strict';

const { version } = require('./package.json');
console.log('\x1b[31m\n                        _____\n                       /  __ \\\n\x1b[33m  ___  _ __   ___ _ __ \x1b[31m| /  \\/ __ _ ___  ___ _ __ _ __   ___\n\x1b[33m / _ \\| \'_ \\ / _ \\ \'_ \\\x1b[31m| |    / _` / __|/ _ \\ \'__| \'_ \\ / _ \\\n\x1b[33m| (_) | |_) |  __/ | | |\x1b[31m \\__/\\ (_| \\__ \\  __/ |  | | | |  __/\n\x1b[33m \\___/| .__/ \\___|_| |_|\x1b[31m\\____/\\__,_|___/\\___|_|  |_| |_|\\___| \x1b[0mv' + version + '\x1b[33m\n\x1b[33m      | |\n\x1b[33m      |_|\n\x1b[0m');

const logger = require('./src/utils/logger')('Launcher');
const server = require('./server.js');

process.on('SIGINT', async () => {
  if(server.isStarted()) {
    console.log('');
    logger.debug('Stopping openCaserne...');
    if(await server.stop()) process.exitCode = 1;
  }
});

logger.debug('Starting openCaserne v', version, '...');
server.start();
