'use strict';

const logger = require('./src/utils/logger')('Server');
const Pack = require('./package');

const Boom = require('@hapi/boom');
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const hapiSwagger = require('hapi-swagger');
const Hapi = require('@hapi/hapi');

const config = require('./src/utils/config');
const configManager = require('./src/utils/configManager');
const rsaJwt = require('./src/utils/rsaJwt');

const fs = require('fs');
const path = require('path');

const routes = require('./src/routes');

let server;
const start = async () => {
	await configManager.check();

	server = Hapi.server({
		host: config.get('api_host'),
		port: config.get('api_port'),
		routes: {
			cors: {
				origin: config.get('api_origin')
			},
			validate: {
				failAction: (request, h, err) => {
					console.log('validationError', err);
				}
			}
		}
	});

	if(config.get('swagger')) {
	  await server.register([
			Inert,
			Vision,
			{
				plugin: hapiSwagger,
				options: {
					info: {
						title: 'OpenCaserne API Documentation',
						description: Pack.description,
						version: Pack.version,
					},
					host: config.get('api_url'),
					schemes: ['http', 'https'],
					securityDefinitions: {
						Bearer: {
							type: 'apiKey',
							name: 'Authorization',
							in: 'header'
						}
					}
				}
			}
		]);
	}

	const rsaJwtInstance = await rsaJwt(config.get('jwt_keypath'));
	server.method('jwtsign', rsaJwtInstance.sign);
	server.auth.scheme('header', (server, options) => {
		if(!options || !options.validate || typeof options.validate !== 'function') {
			throw Boom.badImplementation('Missing validate function');
		}
		options.name = options.name || 'Authorization';
		options.hasPrefix = options.hasPrefix || true;

		return {
			authenticate: (request, h) => {
				const header = request.headers[options.name.toLowerCase()];
				if(!header) {
					return h.unauthenticated(Boom.unauthorized('Can\'t authenticate: no ' + options.name + ' header'));
				}

				const token = header.split(/\s+/)[options.hasPrefix? 1 : 0];
				if(!token) {
					return h.unauthenticated(Boom.unauthorized('Can\'t authenticate: can\'t find token'));
				}

				const {isValid, credentials} = options.validate(token);
				if(!isValid) {
					return h.unauthenticated(Boom.unauthorized('Bad token'));
				}

				return h.authenticated({credentials: credentials});
			}
		}
	});
	server.auth.strategy('bearer', 'header', {
		validate: (token) => {
			const credentials = rsaJwtInstance.verify(token);
			return {isValid: credentials !== false, credentials: credentials};
		}
	});

	server.events.on('response', function (request) {
		const ip = (request.headers['x-real-ip'] && request.info.remoteAddress === '127.0.0.1')? 'x-' + request.headers['x-real-ip'] : request.info.remoteAddress;
		console.log(ip + ': ' + request.method.toUpperCase() + ' ' + request.path + ' --> ' + request.response.statusCode);
	});

	server.events.on('start', function () {
		logger.success('Server running at:', server.info.uri, '(public uri:', config.get('api_url'), ')');
		if(config.get('swagger')) {
			logger.success('Swagger running at:', config.get('api_url')+'/documentation/', '(public uri:', config.get('api_url')+'/documentation/', ')');
		}
	});

	server.events.on('stop', function () {
		logger.success('Server stopped');
	});

	routes.register(server);

	await server.start();
};

module.exports = {
	isStarted: () => server && server.info.started > 0,
	start: async (options) => {
		try {
			await start(options);
		} catch(err) {
			logger.error(err);
			process.exit(1);
		}
	},
	stop: async () => {
		try {
			await server.stop({timeout: 2000});
			server = null;
		}
		catch(err) {
			logger.error(err);
			process.exit(1);
		}
	}
};
