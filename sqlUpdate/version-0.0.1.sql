delimiter //

SET NAMES utf8;//
SET time_zone = '+00:00';//

CREATE FUNCTION `UuidToBin`(_uuid BINARY(36)) RETURNS binary(16)
DETERMINISTIC
SQL SECURITY INVOKER
RETURN
UNHEX(CONCAT(
  SUBSTR(_uuid, 15, 4),
  SUBSTR(_uuid, 10, 4),
  SUBSTR(_uuid,  1, 8),
  SUBSTR(_uuid, 20, 4),
  SUBSTR(_uuid, 25)
));//

CREATE FUNCTION `UuidFromBin`(`_bin` binary(16)) RETURNS binary(36)
  DETERMINISTIC
  SQL SECURITY INVOKER
RETURN
  IF(_bin IS NULL, NULL, LCASE(CONCAT_WS('-',
    HEX(SUBSTR(_bin,  5, 4)),
    HEX(SUBSTR(_bin,  3, 2)),
    HEX(SUBSTR(_bin,  1, 2)),
    HEX(SUBSTR(_bin,  9, 2)),
    HEX(SUBSTR(_bin, 11))
  )));//

CREATE TABLE `version_history` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `version` char(10) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `isCompleted` tinyint(1) NOT NULL DEFAULT '0',
  `error` text NULL
);//

CREATE TABLE `departments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `uuid` binary(16) NOT NULL,
  `name` char(64) NOT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  `address` tinytext DEFAULT NULL,
  UNIQUE KEY `uuid_unique` (`uuid`),
  KEY `parent` (`parent_id`),
  FOREIGN KEY (`parent_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;//

CREATE TRIGGER `departments_bi_uuid` BEFORE INSERT ON `departments` FOR EACH ROW
BEGIN
  IF(NEW.uuid = 0 OR LENGTH(TRIM(NEW.uuid)) <= 0) THEN
    SET NEW.uuid = UuidToBin(UUID());
  END IF;
END;//

CREATE TRIGGER `departments_bi_trim_char` BEFORE INSERT ON `departments` FOR EACH ROW
BEGIN
  IF(LENGTH(TRIM(NEW.name)) > 0) THEN
    SET NEW.name = TRIM(NEW.name);
  ELSE
    SET NEW.name = NULL;
  END IF;

  IF(LENGTH(TRIM(NEW.address)) > 0) THEN
    SET NEW.address = TRIM(NEW.address);
  ELSE
    SET NEW.address = NULL;
  END IF;
END;//

CREATE TABLE `skills` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `uuid` binary(16) NOT NULL,
  `name` char(64) NOT NULL,
  `short_name` char(16) NOT NULL,
  `description` tinytext DEFAULT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  UNIQUE KEY `uuid_unique` (`uuid`),
  UNIQUE KEY `short_name_unique` (`short_name`),
  KEY `parent` (`parent_id`),
  FOREIGN KEY (`parent_id`) REFERENCES `skills` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;//

CREATE TRIGGER `skills_bi_uuid` BEFORE INSERT ON `skills` FOR EACH ROW
BEGIN
  IF(NEW.uuid = 0 OR LENGTH(TRIM(NEW.uuid)) <= 0) THEN
    SET NEW.uuid = UuidToBin(UUID());
  END IF;
END;//

CREATE TRIGGER `skills_bi_trim_char` BEFORE INSERT ON `skills` FOR EACH ROW
BEGIN
  IF(LENGTH(TRIM(NEW.name)) > 0) THEN
    SET NEW.name = TRIM(NEW.name);
  ELSE
    SET NEW.name = NULL;
  END IF;

  IF(LENGTH(TRIM(NEW.short_name)) > 0) THEN
    SET NEW.short_name = TRIM(NEW.short_name);
  ELSE
    SET NEW.short_name = NULL;
  END IF;

  IF(LENGTH(TRIM(NEW.description)) > 0) THEN
    SET NEW.description = TRIM(NEW.description);
  ELSE
    SET NEW.description = NULL;
  END IF;
END;//

CREATE TABLE `staff` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `uuid` binary(16) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `status` enum('WAITING', 'ENABLED', 'ACTIVATED', 'DISABLED', 'DELETED') NOT NULL,
  `username` char(32) NOT NULL,
  `name` char(32) NOT NULL,
  `surname` char(32) NOT NULL,
  `email` char(254) NOT NULL,
  `address` tinytext NOT NULL,
  `password_hash` char(64) NOT NULL,
  `2fa_key` char(64) DEFAULT NULL,
  `has_logged_in` tinyint(1) NOT NULL DEFAULT 0,
  `force_password_update` tinyint(1) NOT NULL DEFAULT 0,
  `department_id` int(11) unsigned NOT NULL,
  UNIQUE KEY `uuid_unique` (`uuid`),
  UNIQUE KEY `username_unique` (`username`),
  UNIQUE KEY `email_unique` (`email`),
  KEY `uuid` (`uuid`),
  KEY `department_id` (`department_id`),
  FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;//

CREATE TRIGGER `staff_bi_uuid` BEFORE INSERT ON `staff` FOR EACH ROW
BEGIN
  IF(NEW.uuid = 0 OR LENGTH(TRIM(NEW.uuid)) <= 0) THEN
    SET NEW.uuid = UuidToBin(UUID());
  END IF;
END;//

CREATE TRIGGER `staff_bi_trim_char` BEFORE INSERT ON `staff` FOR EACH ROW
BEGIN
  IF(LENGTH(TRIM(NEW.username)) > 0) THEN
    SET NEW.name = TRIM(NEW.username);
  ELSE
    SET NEW.username = NULL;
  END IF;

  IF(LENGTH(TRIM(NEW.name)) > 0) THEN
    SET NEW.name = TRIM(NEW.name);
  ELSE
    SET NEW.name = NULL;
  END IF;

  IF(LENGTH(TRIM(NEW.surname)) > 0) THEN
    SET NEW.surname = TRIM(NEW.surname);
  ELSE
    SET NEW.surname = NULL;
  END IF;

  IF(LENGTH(TRIM(NEW.email)) > 0) THEN
    SET NEW.email = TRIM(NEW.email);
  ELSE
    SET NEW.email = NULL;
  END IF;

  IF(LENGTH(TRIM(NEW.address)) > 0) THEN
    SET NEW.address = TRIM(NEW.address);
  ELSE
    SET NEW.address = NULL;
  END IF;

  IF(LENGTH(TRIM(NEW.password_hash)) > 0) THEN
    SET NEW.password_hash = TRIM(NEW.password_hash);
  ELSE
    SET NEW.password_hash = NULL;
  END IF;

  IF(LENGTH(TRIM(NEW.2fa_key)) > 0) THEN
    SET NEW.2fa_key = TRIM(NEW.2fa_key);
  ELSE
    SET NEW.2fa_key = NULL;
  END IF;
END;//

CREATE TABLE `staff_skills` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `uuid` binary(16) NOT NULL,
  `staff_id` int(11) unsigned NOT NULL,
  `skill_id` int(11) unsigned NOT NULL,
  UNIQUE KEY `staff_id_skill_id_unique` (`staff_id`, `skill_id`),
  FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`skill_id`) REFERENCES `skills` (`id`)
);//

CREATE TRIGGER `staff_skills_bi_uuid` BEFORE INSERT ON `staff_skills` FOR EACH ROW
BEGIN
  IF(NEW.uuid = 0 OR LENGTH(TRIM(NEW.uuid)) <= 0) THEN
    SET NEW.uuid = UuidToBin(UUID());
  END IF;
END;//

CREATE TABLE `events` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `uuid` binary(16) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `creator_id` int(11) unsigned NOT NULL,
  `type` enum('POSTE', 'FORMATION', 'OTHER') NOT NULL,
  `name` char(64) NOT NULL,
  `description` text DEFAULT NULL,
  `required_quantity` smallint unsigned DEFAULT NULL,
  `place` text DEFAULT NULL,
  `is_open` tinyint(1) NOT NULL,
  `is_visible` tinyint(1) NOT NULL,
  `begin_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `appointment_place` text DEFAULT NULL,
  `appointment_date` timestamp NULL DEFAULT NULL,
  `manager_id` int(11) unsigned NOT NULL,
  `department_id` int(11) unsigned NOT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL,
  UNIQUE KEY `uuid_unique` (`uuid`),
  KEY `manager` (`manager_id`),
  KEY `parent` (`parent_id`),
  KEY `department_id` (`department_id`),
  KEY `creator_id` (`creator_id`),
  FOREIGN KEY (`manager_id`) REFERENCES `staff` (`id`) ON UPDATE CASCADE,
  FOREIGN KEY (`parent_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON UPDATE CASCADE,
  FOREIGN KEY (`creator_id`) REFERENCES `staff` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;//

CREATE TRIGGER `events_bi_uuid` BEFORE INSERT ON `events` FOR EACH ROW
BEGIN
  IF(NEW.uuid = 0 OR LENGTH(TRIM(NEW.uuid)) <= 0) THEN
    SET NEW.uuid = UuidToBin(UUID());
  END IF;
END;//

CREATE TRIGGER `events_bi_trim_char` BEFORE INSERT ON `events` FOR EACH ROW
BEGIN
  IF(LENGTH(TRIM(NEW.name)) > 0) THEN
    SET NEW.name = TRIM(NEW.name);
  ELSE
    SET NEW.name = NULL;
  END IF;

  IF(LENGTH(TRIM(NEW.description)) > 0) THEN
    SET NEW.description = TRIM(NEW.description);
  ELSE
    SET NEW.description = NULL;
  END IF;

  IF(LENGTH(TRIM(NEW.place)) > 0) THEN
    SET NEW.place = TRIM(NEW.place);
  ELSE
    SET NEW.place = NULL;
  END IF;

  IF(LENGTH(TRIM(NEW.appointment_place)) > 0) THEN
    SET NEW.appointment_place = TRIM(NEW.appointment_place);
  ELSE
    SET NEW.appointment_place = NULL;
  END IF;
END;//

CREATE TABLE `events_skills` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `uuid` binary(16) NOT NULL,
  `event_id` int(11) unsigned NOT NULL,
  `skill_id` int(11) unsigned NOT NULL,
  `required_quantity` smallint unsigned NOT NULL,
  UNIQUE KEY `event_id_skill_id_unique` (`event_id`, `skill_id`),
  FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`skill_id`) REFERENCES `skills` (`id`)
);//

CREATE TRIGGER `events_skills_bi_uuid` BEFORE INSERT ON `events_skills` FOR EACH ROW
BEGIN
  IF(NEW.uuid = 0 OR LENGTH(TRIM(NEW.uuid)) <= 0) THEN
    SET NEW.uuid = UuidToBin(UUID());
  END IF;
END;//

CREATE TABLE `events_staff` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `uuid` binary(16) NOT NULL,
  `staff_id` int(11) unsigned NOT NULL,
  `event_id` int(11) unsigned NOT NULL,
  `skill_id` int(11) unsigned DEFAULT NULL,
  KEY `staff_id` (`staff_id`),
  KEY `event_id` (`event_id`),
  FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE,
  FOREIGN KEY (`skill_id`) REFERENCES `skills` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;//

CREATE TRIGGER `events_staff_bi_uuid` BEFORE INSERT ON `events_staff` FOR EACH ROW
BEGIN
  IF(NEW.uuid = 0 OR LENGTH(TRIM(NEW.uuid)) <= 0) THEN
    SET NEW.uuid = UuidToBin(UUID());
  END IF;
END;//

CREATE TABLE `news` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `uuid` binary(16) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `creator_id` int(11) unsigned NOT NULL,
  `content` text NOT NULL,
  `department_id` int(11) unsigned NOT NULL,
  UNIQUE KEY `uuid_unique` (`uuid`),
  KEY `department_id` (`department_id`),
  KEY `creator_id` (`creator_id`),
  FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON UPDATE CASCADE,
  FOREIGN KEY (`creator_id`) REFERENCES `staff` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;//

CREATE TRIGGER `news_bi_uuid` BEFORE INSERT ON `news` FOR EACH ROW
BEGIN
  IF(NEW.uuid = 0 OR LENGTH(TRIM(NEW.uuid)) <= 0) THEN
    SET NEW.uuid = UuidToBin(UUID());
  END IF;
END;//

CREATE TRIGGER `news_bi_trim_char` BEFORE INSERT ON `news` FOR EACH ROW
BEGIN
  IF(LENGTH(TRIM(NEW.content)) > 0) THEN
    SET NEW.content = TRIM(NEW.content);
  ELSE
    SET NEW.content = NULL;
  END IF;
END;//

INSERT INTO `departments` (`name`, `parent_id`) VALUES
('Fédération', NULL),
('Association 1', 1),
('Antenne 1-1', 2),
('Antenne 1-2', 2),
('Antenne 1-3', 2),
('Association 2', 1),
('Antenne 2-1', 6),
('Antenne 2-2', 6),
('Association 3', 1),
('Antenne 3-1', 9);//

INSERT INTO `staff` (`status`, `username`, `name`, `surname`, `email`, `address`, `password_hash`, `force_password_update`, `department_id`) VALUES
('ACTIVATED', 'openCaserne', 'Admin', 'Admin', 'admin@opencaserne.fr', 'France', '$2b$10$5hj/21D0IZSNCYZun94JkeAZuGiVq2Cg6i7lSrbxGX48sPbl8myA2', 1, 1),
('ACTIVATED', 'vlortet', 'Valentin', 'Lortet', 'valentin@lortet.fr', 'France', '$2b$10$5hj/21D0IZSNCYZun94JkeAZuGiVq2Cg6i7lSrbxGX48sPbl8myA2', 1, 3);//

INSERT INTO `events` (`creator_id`, `type`, `name`, `description`, `required_quantity`, `place`, `is_open`, `is_visible`, `begin_date`, `end_date`, `manager_id`, `department_id`, `parent_id`) VALUES
(2,	'POSTE',	'Jeux Olympiques',	'Événement global pour les jeux olympiques',	NULL, NULL,	0,	1,	'2024-07-25 22:00:00',	'2024-08-11 21:59:59',	1,	1,	NULL),
(2,	'POSTE',	'Cérémonie de fermeture - Jeux olympiques',	NULL,	5, 'Place de la Concorde, 75008 Paris',	1,	1,	'2024-08-11 08:00:00',	'2024-08-11 15:00:00',	1,	1,	1),
(2,	'POSTE',	'Cérémonie d\'ouverture - Jeux olympiques',	NULL,	5, 'Stade de France, 93200 Saint-Denis',	1,	1,	'2024-07-26 08:00:00',	'2024-07-26 15:00:00',	1,	1,	1),
(2,	'FORMATION',	'PSC1',	'Amener son repas. Pause d\'une heure le midi.',	12, 'Local de Couëron',	1,	1,	'2020-06-19 06:00:00',	'2020-06-19 16:00:00',	1,	1,	NULL),
(2,	'OTHER',	'Repas annuel',	NULL,	100, 'Local de Nantes',	1,	1,	'2019-12-20 16:30:00',	'2019-12-20 22:59:00',	1,	1,	NULL);//

INSERT INTO `news` (`creator_id`, `content`, `department_id`) VALUES
(2,	'Lorem ipsum dolor sit amet,\r\nconsectetur adipiscing elit. Mauris porttitor volutpat ipsum quis bibendum. Donec sodales sem bibendum, aliquam justo non, accumsan massa. In vitae lacus fermentum, elementum lacus a, gravida magna. In consectetur massa vitae est suscipit, nec aliquet nulla consectetur. Nam efficitur pulvinar blandit. Morbi vitae ex ut odio dapibus imperdiet sit amet quis justo. Nunc ornare, augue at ultricies venenatis, justo risus aliquam nibh, bibendum congue nibh sem a elit. Aliquam sollicitudin congue maximus. Cras tempor euismod dui sed laoreet. Donec interdum nibh fermentum urna gravida scelerisque. Morbi sollicitudin odio et eleifend volutpat.\r\n\r\nSed ultricies lectus id quam suscipit, sit amet dictum lorem commodo.',	1),
(2,	'Bonjour,\r\n\r\nL\'objectif de cette plate-forme est d\'essayer de moderniser eBrigade/eProtec.',	1);//

INSERT INTO `skills` (`name`, `short_name`, `description`, `parent_id`) VALUES
('Stagiaire',	'stag.',	NULL,	NULL),
('Logisticien Administratif et Technique',	'LAT',	NULL,	1),
('Prévention et Secours Civiques de Niveau 1',	'PSC1',	NULL,	2),
('Premiers Secours en Équipe de Niveau 1',	'PSE1',	NULL,	3),
('Premiers Secours en Équipe de Niveau 2',	'PSE2',	NULL,	4),
('Chef d\'Équipe',	'CE',	NULL,	5),
('Chef de Poste',	'CP',	NULL,	6),
('Chef de Secteur',	'CS',	NULL,	6),
('Chef de Dispositif',	'CDD',	NULL,	7),
('Jeune Sapeur-Pompier',	'JSP',	NULL,	NULL),
('Sapeur-Pompier',	'SP',	NULL,	10),
('Aide Soignant',	'A. Soign.',	NULL,	NULL),
('Infirmier',	'Inf.',	NULL,	12),
('Sauveteur Secouriste du Travail',	'SST',	NULL,	2),
('Formateur Sauveteur Secouriste du Travail',	'Form. SST',	NULL,	14),
('Formateur Prévention et Secours Civiques de Niveau 1',	'Form. PSC1',	NULL,	3),
('Formateur Premiers Secours en Équipe',	'Form. PSE',	NULL,	5),
('Medecin',	'Med.',	NULL,	13),
('Permis B',	'Perm. B',	NULL,	NULL),
('Permis C1',	'Perm. C1',	'',	19),
('Permis D1',	'Perm. D1',	'',	19),
('Permis BE',	'Perm. BE',	NULL,	19),
('Permis C',	'Perm. C',	NULL,	20),
('Permis D',	'Perm. D',	NULL,	21),
('Permis A1',	'Perm. A1',	NULL,	NULL),
('Permis A Progressif',	'Perm. A Prog.',	NULL,	25),
('Permis A Direct',	'Perm. A Dir.',	NULL,	26),
('Permis CE',	'Perm. CE',	NULL,	23),
('Permis DE',	'Perm. DE',	NULL,	24),
('Brevet National de Sécurité et de Sauvetage Aquatique',	'BNSSA',	NULL,	4);//

INSERT INTO `events_skills` (`event_id`, `skill_id`, `required_quantity`) VALUES
(2, 7, 1),
(2, 5, 3),
(2, 4, 1),
(2, 1, 1),
(3, 7, 1),
(3, 5, 3),
(3, 4, 1),
(3, 1, 1),
(4, 16, 2);//

INSERT INTO `staff_skills` (`staff_id`, `skill_id`) VALUES
(2, 5),
(2, 19);//

INSERT INTO `events_staff` (`event_id`, `staff_id`, `skill_id`) VALUES
(3, 2, NULL),
(4, 2, NULL);//
