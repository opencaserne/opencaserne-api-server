'use strict';

const Joi = require('@hapi/joi');
const eventsController = require('../controllers/eventsController');
const eventsModel = require('../models/eventsModel');

module.exports = function() {

	return [
		{
			method: 'GET',
			path: '/events',
			handler: eventsController.find,
			options: {
				description: 'Get events list',
				tags: ['api'],
				response: {
					schema: Joi.array().items(eventsModel.schemas.get).label('eventsList')
				},
				auth: {
					strategy: 'bearer',
					scope: ['type-user']
				}
			}
		},
		{
			method: 'GET',
			path: '/events/{eventUuid}',
			handler: eventsController.findByEventUuid,
			options: {
				description: 'Get a events',
				tags: ['api'],
				validate: {
					params: {
						eventUuid: eventsModel.schemas.RAW.uuid
					}
				},
				response: {
					schema: eventsModel.schemas.get
				},
				auth: {
					strategy: 'bearer',
					scope: ['type-user']
				}
			}
		}
	];
}();
