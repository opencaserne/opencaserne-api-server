'use strict';

const Joi = require('@hapi/joi');
const newsController = require('../controllers/newsController');
const newsModel = require('../models/newsModel');

module.exports = function() {

	return [
		{
			method: 'GET',
			path: '/news',
			handler: newsController.find,
			options: {
				description: 'Get news list',
				tags: ['api'],
				response: {
					schema: Joi.array().items(newsModel.schemas.get).label('newsList')
				},
				auth: {
					strategy: 'bearer',
					scope: ['type-user']
				}
			}
		},
		{
			method: 'GET',
			path: '/news/{newsUuid}',
			handler: newsController.findByNewsUuid,
			options: {
				description: 'Get a news',
				tags: ['api'],
				validate: {
					params: {
						newsUuid: newsModel.schemas.RAW.uuid
					}
				},
				response: {
					schema: newsModel.schemas.get
				},
				auth: {
					strategy: 'bearer',
					scope: ['type-user', 'new-{params.newsUuid}']
				}
			}
		}
	];
}();
