'use strict';

const Joi = require('@hapi/joi');
const skillsController = require('../controllers/skillsController');
const skillsModel = require('../models/skillsModel');

module.exports = function() {

	return [
		{
			method: 'GET',
			path: '/skills',
			handler: skillsController.find,
			options: {
				description: 'Get skills list',
				tags: ['api'],
				response: {
					schema: Joi.array().items(skillsModel.schemas.get).label('skillsList')
				},
				auth: {
					strategy: 'bearer',
					scope: ['type-user']
				}
			}
		},
		{
			method: 'GET',
			path: '/skills/{skillUuid}',
			handler: skillsController.findByskillUuid,
			options: {
				description: 'Get a skills',
				tags: ['api'],
				validate: {
					params: {
						skillUuid: skillsModel.schemas.RAW.uuid
					}
				},
				response: {
					schema: skillsModel.schemas.get
				},
				auth: {
					strategy: 'bearer',
					scope: ['type-user']
				}
			}
		}
	];
}();
