'use strict';

const Joi = require('@hapi/joi');
const authController = require('../controllers/authController');
const helpModel = require('../models/helpModel');
const authModel = require('../models/authModel');
const staffModel = require('../models/staffModel');

module.exports = [
	{
		method: 'POST',
		path: '/auth/session',
		handler: authController.logIn,
		options: {
			description: 'Log in',
			tags: ['api', 'auth'],
			validate: {
				payload: {
					emailOrUsername: [staffModel.schemas.RAW.username.required(), helpModel.rows.email.required()],
					password: staffModel.schemas.RAW.password.required()
				}
			},
			response: {
				schema: authModel.schemas.get
			},
			auth: false
		}
	},
	{
		method: 'DELETE',
		path: '/auth/session',
		handler: authController.logOut,
		options: {
			description: 'Log out',
			tags: ['api', 'auth'],
			auth: {
				strategy: 'bearer'
			}
		}
	},
	{
		method: 'POST',
		path: '/auth/password/reset',
		handler: authController.passwordReset,
		options: {
			description: 'Request password update',
			tags: ['api'],
			auth: false
		}
	},
	{
		method: 'POST',
		path: '/auth/password/update',
		handler: authController.passwordUpdate,
		options: {
			description: 'Update password',
			tags: ['api'],
			auth: {
				strategy: 'bearer'
			}
		}
	}
];
