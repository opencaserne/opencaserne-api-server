'use strict';

const Joi = require('@hapi/joi');
const departmentsController = require('../controllers/departmentsController');
const departmentsModel = require('../models/departmentsModel');

module.exports = function() {

	return [
		{
			method: 'GET',
			path: '/departments',
			handler: departmentsController.find,
			options: {
				description: 'Get departments list',
				tags: ['api'],
				response: {
					schema: Joi.array().items(departmentsModel.schemas.get).label('departmentsList')
				},
				auth: {
					strategy: 'bearer',
					scope: ['type-user']
				}
			}
		},
		{
			method: 'GET',
			path: '/departments/{departmentUuid}',
			handler: departmentsController.findByDepartmentUuid,
			options: {
				description: 'Get a departments',
				tags: ['api'],
				validate: {
					params: {
						departmentUuid: departmentsModel.schemas.RAW.uuid
					}
				},
				response: {
					schema: departmentsModel.schemas.get
				},
				auth: {
					strategy: 'bearer',
					scope: ['type-user', 'department-{params.departmentUuid}']
				}
			}
		}
	];
}();
