'use strict';

const logger = require('../utils/logger')(module.filename);
const fs = require('fs');
const path = require('path');

const filesExports = [];
fs.readdirSync(__dirname).forEach(function (file) {
	if ('index.js' === file || fs.statSync(path.join(__dirname, file)).isDirectory()) return;
	filesExports.push(require(path.join(__dirname, file)));
});

module.exports = {
	register: (server) => {
		filesExports.forEach((file) => {
			server.route(file);
		});
	}
};
