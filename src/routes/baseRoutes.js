'use strict';

const Joi = require('@hapi/joi');
const baseController = require('../controllers/baseController');
const helpModel = require('../models/helpModel');

module.exports = [
	{
		method: 'GET',
		path: '/',
		handler: baseController.list,
		options: {
			description: 'Get base api list',
			tags: ['api'],
			response: {
				schema: helpModel.rows.links
			},
			auth: false
		}
	}
];
