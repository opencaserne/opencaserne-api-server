'use strict';

const Joi = require('@hapi/joi');
const staffController = require('../controllers/staffController');
const staffModel = require('../models/staffModel');

module.exports = function() {

	return [
		{
			method: 'GET',
			path: '/staff',
			handler: staffController.find,
			options: {
				description: 'Get staff list',
				tags: ['api'],
				response: {
					schema: Joi.array().items(staffModel.schemas.get).label('staffList')
				},
				auth: {
					strategy: 'bearer',
					scope: ['type-user']
				}
			}
		},
		{
			method: 'GET',
			path: '/staff/{staffUuid}',
			handler: staffController.findByStaffUuid,
			options: {
				description: 'Get a staff',
				tags: ['api'],
				validate: {
					params: {
						staffUuid: staffModel.schemas.RAW.uuid
					}
				},
				response: {
					schema: staffModel.schemas.get
				},
				auth: {
					strategy: 'bearer',
					scope: ['type-user', 'user-{params.staffUuid}']
				}
			}
		}
	];
}();
