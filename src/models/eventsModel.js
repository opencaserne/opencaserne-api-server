'use strict';

const Joi = require('@hapi/joi');
const SchemaCompilation = require('../utils/SchemaCompilation.js');

const helpModel = require('./helpModel');

const structure = {
	uuid: {schema: helpModel.rows.uuid, categories: ['!get', 'insert'], sql: {function: {get: 'UuidFromBin', insert: 'UuidToBin'}}},
	creationDate: {schema: helpModel.rows.timestamp, categories: ['!get']},
	creatorUuid: {schema: helpModel.rows.uuid, categories: ['!get', '!insert'], sql: {function: {get: 'UuidFromBin', insert: 'UuidFromBin'}, join: {column: 'uuid', table: 'staff', alias: 'creator', condition: 'events.creator_id = creator.id'}}},
	creatorName: {schema: Joi.string(), categories: ['!get'], sql: {join: {column: 'name', table: 'staff', alias: 'creator', condition: 'events.creator_id = creator.id'}}},
	type: {schema: Joi.string(), categories: ['!get', '!put', '!insert']},
	name: {schema: Joi.string(), categories: ['!get', '!put', '!insert']},
	description: {schema: Joi.string(), categories: ['get', 'put', 'insert']},
	requiredQuantity: {schema: Joi.number(), categories: ['!get', 'put', 'insert']},
	place: {schema: Joi.string(), categories: ['get', 'put', 'insert']},
	isOpen: {schema: Joi.boolean(), categories: ['!get', 'put', 'insert']},
	isVisible: {schema: Joi.boolean(), categories: ['!get', 'put', 'insert']},
	beginDate: {schema: helpModel.rows.timestamp, categories: ['!get', '!put', '!insert']},
	endDate: {schema: helpModel.rows.timestamp, categories: ['!get', '!put', '!insert']},
	appointmentPlace: {schema: Joi.string(), categories: ['get', 'put', 'insert']},
	appointmentDate: {schema: helpModel.rows.timestamp, categories: ['get', 'put', 'insert']},
	skillsRequirement: {schema: Joi.any(), categories: ['!get'], sql: {
		subquery: 'SELECT CONCAT(\'[\', GROUP_CONCAT(CONCAT(\'{"skill":{"uuid":"\', UuidFromBin(s.uuid), \'", "name":"\', s.name, \'", "shortName":"\', s.short_name, \'"\', IFNULL(CONCAT(\', "description":"\', s.description, \'"\'), \'\'), \'}, "requiredQuantity":\', required_quantity, \'}\') SEPARATOR \', \'), \']\') skills FROM events_skills JOIN skills s ON s.id = skill_id WHERE event_id = events.id'
	}},
	staff: {schema: Joi.any(), categories: ['!get'], sql: {
		subquery: 'SELECT CONCAT(\'[\', GROUP_CONCAT(CONCAT(\'{"uuid":"\', UuidFromBin(staff.uuid), \'", "username":"\', staff.username, \'", "name":"\', staff.name, \'", "surname":"\', staff.surname, \'"\', IFNULL(CONCAT(\', "skill":"\', UuidFromBin(s.uuid), \'"\'), \'\'), \', "skills":[\', IFNULL((SELECT GROUP_CONCAT(CONCAT(\'{"uuid":"\', UuidFromBin(s.uuid), \'", "name":"\', s.name, \'", "shortName":"\', s.short_name, \'"\', IFNULL(CONCAT(\', "description":"\', s.description, \'"\'), \'\'), \'}\') SEPARATOR \', \') FROM staff_skills JOIN skills s ON s.id = skill_id WHERE staff_skills.staff_id = events_staff.staff_id), \'\'), \']}\') SEPARATOR \', \'), \']\') FROM events_staff JOIN staff ON staff.id = staff_id LEFT JOIN skills s ON s.id = events_staff.skill_id WHERE event_id = events.id'
	}},
	managerUuid: {schema: helpModel.rows.uuid, categories: ['!get', '!put', '!insert'], sql: {function: {get: 'UuidFromBin', insert: 'UuidToBin'}, join: {column: 'uuid', table: 'staff', alias: 'manager', condition: 'events.manager_id = manager.id'}}},
	managerName: {schema: Joi.string(), categories: ['!get'], sql: {join: {column: 'name', table: 'staff', alias: 'manager', condition: 'events.manager_id = manager.id'}}},
	departmentUuid: {schema: helpModel.rows.uuid, categories: ['!get', '!put', '!insert'], sql: {function: {get: 'UuidFromBin', insert: 'UuidToBin'}, join: {column: 'uuid', table: 'departments', alias: 'department', condition: 'events.department_id = department.id'}}},
	departmentName: {schema: Joi.string(), categories: ['!get'], sql: {join: {column: 'name', table: 'departments', alias: 'department', condition: 'events.department_id = department.id'}}},
	parentUuid: {schema: helpModel.rows.uuid, categories: ['get', 'put', 'insert'], sql: {function: {get: 'UuidFromBin', insert: 'UuidToBin'}, join: {column: 'uuid', table: 'events', alias: 'parent', condition: 'events.parent_id = parent.id'}}},
	parentName: {schema: Joi.string(), categories: ['get'], sql: {join: {column: 'name', table: 'events', alias: 'parent', condition: 'events.parent_id = parent.id'}}},
	links: {schema: helpModel.rows.links, categories: ['!get'], sql: false}
};

const schemas = SchemaCompilation.toSchemas(structure, {get: 'Get Department Object', put: 'Put Department Object'});
const sqlQueries = {
	select: SchemaCompilation.toSqlQuerie(structure, 'select', 'get', 'events'),
	insert: SchemaCompilation.toSqlQuerie(structure, 'insert', 'insert', 'events')
};

console.log(sqlQueries.select);

const schemaFromDao = (dao, linker) => {
	return {
		uuid: '' + dao.uuid,
		creationDate: Math.round(new Date(dao.creation_date).getTime()/1000),
		creatorUuid: dao.creator_uuid? '' + dao.creator_uuid : undefined,
		creatorName: dao.creator_name || undefined,
		type: dao.type,
		name: dao.name,
		description: dao.description || undefined,
		requiredQuantity: dao.required_quantity || 0,
		place: dao.place || undefined,
		isOpen: Boolean(dao.is_open),
		isVisible: Boolean(dao.is_visible),
		beginDate: Math.round(new Date(dao.begin_date).getTime()/1000),
		endDate: Math.round(new Date(dao.end_date).getTime()/1000),
		appointmentPlace: dao.appointment_place || undefined,
		appointmentDate: dao.appointment_date? Math.round(new Date(dao.appointment_date).getTime()/1000) : undefined,
		skillsRequirement: dao.skills_requirement? JSON.parse(dao.skills_requirement) : [],
		staff: dao.staff? JSON.parse(dao.staff) : [],
		managerUuid: dao.manager_uuid? '' + dao.manager_uuid : undefined,
		managerName: dao.manager_name || undefined,
		departmentUuid: dao.department_uuid? '' + dao.department_uuid : undefined,
		departmentName: dao.department_name || undefined,
		parentUuid: dao.parent_uuid? '' + dao.parent_uuid : undefined,
		parentName: dao.parent_name || undefined,
		links: linker? linker.makeLinks({
			SELF: ['GET', '/events/{eventUuid}', {params: {eventUuid: dao.uuid}}],
			CREATOR: ['GET', '/staff/{staffUuid}', {params: {staffUuid: dao.creator_uuid}}],
			MANAGER: ['GET', '/staff/{staffUuid}', {params: {staffUuid: dao.manager_uuid}}],
			DEPARTMENT: ['GET', '/departments/{departmentUuid}', {params: {departmentUuid: dao.department_uuid}}],
			PARENT: ['GET', '/events/{eventUuid}', {params: {eventUuid: dao.parent_uuid}}]
		}) : null
	};
};

module.exports = {
	schemas: schemas,
	sqlQueries: sqlQueries,
	schemaFromDao: schemaFromDao,
	schemasFromDaos: function(dao, linker) {
    return dao.reduce((acc, val) => {
      acc.push(schemaFromDao(val, linker));
	   	return acc;
    }, []);
	}
};
