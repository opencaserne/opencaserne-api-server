'use strict';

const Joi = require('@hapi/joi');
const SchemaCompilation = require('../utils/SchemaCompilation.js');

const helpModel = require('./helpModel');

const structure = {
	uuid: {schema: helpModel.rows.uuid, categories: ['!get', 'insert'], sql: {function: {get: 'UuidFromBin', insert: 'UuidToBin'}}},
	name: {schema: Joi.string(), categories: ['!get', '!put', '!insert']},
	shortName: {schema: Joi.string(), categories: ['!get', '!put', '!insert']},
	description: {schema: Joi.string(), categories: ['get', 'put', 'insert']},
	parentUuid: {schema: helpModel.rows.uuid, categories: ['get', 'put', 'insert'], sql: {function: {get: 'UuidFromBin'}, join: {column: 'uuid', table: 'skills', alias: 'parent', condition: 'skills.parent_id = parent.id'}}},
	parentName: {schema: Joi.string(), categories: ['get'], sql: {join: {column: 'name', table: 'skills', alias: 'parent', condition: 'skills.parent_id = parent.id'}}},
	level: {schema: Joi.number(), categories: ['get'], sql: false},
	links: {schema: helpModel.rows.links, categories: ['!get'], sql: false}
};

const schemas = SchemaCompilation.toSchemas(structure, {get: 'Get skill Object', put: 'Put skill Object'});
const sqlQueries = {
	select: SchemaCompilation.toSqlQuerie(structure, 'select', 'get', 'skills'),
	insert: SchemaCompilation.toSqlQuerie(structure, 'insert', 'insert', 'skills'),
	selectRecursive : 'WITH RECURSIVE ancestors(id, uuid, name, short_name, description, level, parent_uuid, path) AS ( ' +
		'SELECT id, uuid, name, short_name, description, 0, CAST(NULL AS BINARY(16)), CAST(name AS CHAR(255)) FROM `skills` WHERE parent_id IS NULL ' +
		'UNION ' +
		'SELECT d.id, d.uuid, d.name, d.short_name, d.description, a.level+1, a.uuid, CONCAT(a.path, \'/\', d.name) FROM `skills` AS d, `ancestors` AS a WHERE d.parent_id = a.id ' +
	') ' +
	'SELECT UuidFromBin(uuid) as uuid, name, short_name, description, level, UuidFromBin(parent_uuid) as parent_uuid FROM ancestors ORDER BY path'
};

const schemaFromDao = (dao, linker) => {
	return {
		uuid: '' + dao.uuid,
		name: dao.name,
		shortName: dao.short_name,
		description: dao.description || undefined,
		name: dao.name,
		parentUuid: dao.parent_uuid? '' + dao.parent_uuid : undefined,
		parentName: dao.parent_name || undefined,
		level: Number.isInteger(dao.level)? dao.level : undefined,
		links: linker.makeLinks({
			SELF: ['GET', '/skills/{skillUuid}', {params: {skillUuid: dao.uuid}}],
			PARENT: ['GET', '/skills/{skillUuid}', {params: {skillUuid: dao.parent_uuid}}]
		})
	};
};

module.exports = {
	schemas: schemas,
	sqlQueries: sqlQueries,
	schemaFromDao: schemaFromDao,
	schemasFromDaos: function(dao, linker) {
    return dao.reduce((acc, val) => {
      acc.push(schemaFromDao(val, linker));
	   	return acc;
    }, []);
	}
};
