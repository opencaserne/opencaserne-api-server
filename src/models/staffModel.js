'use strict';

const Joi = require('@hapi/joi');
const SchemaCompilation = require('../utils/SchemaCompilation.js');

const helpModel = require('./helpModel');

const structure = {
	//count: {schema: Joi.any(), categories: ['!get'], sql: {
	//	function: 'COUNT',
	//	column: '*'
	//}},
	uuid: {schema: helpModel.rows.uuid, categories: ['!get', '!auth', 'insert'], sql: {function: {get: 'UuidFromBin', auth: 'UuidFromBin', insert: 'UuidToBin'}}},
	creationDate: {schema: helpModel.rows.timestamp, categories: ['!get', '!auth']},
	status: {schema: Joi.string(), categories: ['!get', '!auth', '!put', '!insert']},
	username: {schema: Joi.string().alphanum().min(4).max(32), categories: ['!get', '!auth', '!put', '!insert']},
	name: {schema: Joi.string(), categories: ['!get', '!auth', '!put', '!insert']},
	surname: {schema: Joi.string(), categories: ['!get', '!auth', '!put', '!insert']},
	email: {schema: helpModel.rows.email, categories: ['!get', '!auth', '!put', '!insert']},
	address: {schema: Joi.string(), categories: ['get', 'auth', 'put', 'insert']},
	skills: {schema: Joi.any(), categories: ['!get'], sql: {
		subquery: 'SELECT CONCAT(\'[\', GROUP_CONCAT(CONCAT(\'{"uuid":"\', UuidFromBin(s.uuid), \'", "name":"\', s.name, \'", "shortName":"\', s.short_name, \'"\', IFNULL(CONCAT(\', "description":"\', s.description, \'"\'), \'\'), \'}\') SEPARATOR \', \'), \']\') skills FROM staff_skills JOIN skills s ON s.id = skill_id WHERE staff_id = staff.id'
	}},
	passwordHash: {schema: Joi.string(), categories: ['!auth', '!insert']},
	password: {schema: Joi.string(), categories: ['!put'], sql: false},
	'2faKey': {schema: Joi.string(), categories: ['auth', 'put', 'insert']},
	hasLoggedIn: {schema: Joi.boolean(), categories: ['!get', '!auth', 'put', 'insert']},
	forcePasswordUpdate: {schema: Joi.boolean(), categories: ['!get', '!auth', 'put', 'insert']},
	departmentUuid: {schema: helpModel.rows.uuid, categories: ['!get', '!auth', '!put', '!insert'], sql: {function: {get: 'UuidFromBin', auth: 'UuidFromBin', put: 'UuidToBin', insert: 'UuidToBin'}, join: {column: 'uuid', table: 'departments', alias: 'department', condition: 'staff.department_id = department.id'}}},
	departmentName: {schema: Joi.string(), categories: ['!get', '!auth'], sql: {join: {column: 'name', table: 'departments', alias: 'department', condition: 'staff.department_id = department.id'}}},
	links: {schema: helpModel.rows.links, categories: ['!get', '!auth'], sql: false}
};

const schemas = SchemaCompilation.toSchemas(structure, {get: 'Get Staff Object', put: 'Put Staff Object'});
const sqlQueries = {
	select: SchemaCompilation.toSqlQuerie(structure, 'select', 'get', 'staff'),
	authSelect: SchemaCompilation.toSqlQuerie(structure, 'select', 'auth', 'staff'),
	insert: SchemaCompilation.toSqlQuerie(structure, 'insert', 'insert', 'staff')
};

const schemaFromDao = (dao, linker) => {

	console.log(dao.count);

	return {
		uuid: '' + dao.uuid,
		creationDate: dao.creation_date? Math.round(new Date(dao.creation_date).getTime()/1000) : undefined,
		status: dao.status,
		username: dao.username,
		name: dao.name,
		surname: dao.surname,
		email: dao.email,
		address: dao.address || undefined,
		skills: dao.skills? JSON.parse(dao.skills) : [],
		hasLoggedIn: Boolean(dao.has_logged_in),
		forcePasswordUpdate: Boolean(dao.force_password_update),
		departmentUuid: '' + dao.department_uuid,
		departmentName: dao.department_name,
		links: linker.makeLinks({
			SELF: ['GET', '/staff/{staffUuid}', {params: {staffUuid: dao.uuid}}],
			DEPARTMENT: ['GET', '/departments/{departmentUuid}', {params: {departmentUuid: dao.department_uuid}}]
		})
	};
};

module.exports = {
	schemas: schemas,
	sqlQueries: sqlQueries,
	schemaFromDao: schemaFromDao,
	schemasFromDaos: function(dao, linker) {
    return dao.reduce((acc, val) => {
      acc.push(schemaFromDao(val, linker));
	   	return acc;
    }, []);
	}
};
