'use strict';

const Joi = require('@hapi/joi');
const SchemaCompilation = require('../utils/SchemaCompilation.js');

const helpModel = require('./helpModel');
const staffModel = require('./staffModel');

const structure = {
	apiKey: {schema: Joi.string(), categories: ['!get']},
	staff: {schema: staffModel.schemas.get, categories: ['!get']},
	links: {schema: helpModel.rows.links, categories: ['!get']}
};
const schemas = SchemaCompilation.toSchemas(structure, {get: 'Get Auth Object'});

const schemaFromTokenAndUser = (apiKey, staff, linker) => {
	return {
		apiKey: apiKey,
		staff: staff,
		links: linker.makeLinks({
			SELF: ['GET', '/staff/{staffUuid}', {params: {staffUuid: staff.uuid}}],
			NEWS: ['GET', '/news'],
			DEPARTMENTS: ['GET', '/departments'],
			SKILLS: ['GET', '/skills'],
			STAFF: ['GET', '/staff'],
			EVENTS: ['GET', '/events']
		})
	};
};

module.exports = {
  schemas: schemas,
	schemaFromTokenAndUser: schemaFromTokenAndUser
};
