'use strict';

const Joi = require('@hapi/joi');

const rows = {
	id: Joi.number().integer().example(12).description('ressource id'),
	uuid: Joi.string().guid().example('913d93df-273d-4de3-b638-d07f10f6d8a8').description('UUID'),
	links: Joi.object({}).pattern(/\w/, Joi.object({isAuthRoute: Joi.boolean(), href: Joi.string().required(), method: Joi.string().valid('GET', 'POST', 'PUT', 'PATCH', 'DELETE').required(), auth: Joi.string()})).example({SELF:{href: '/xxx', method: 'GET', auth: 'bearer'}}).description('links object'),
	timestamp: Joi.date().timestamp().example('1535665471').description('timestamp').label('timestamp'),
	email: Joi.string().email({minDomainSegments: 2}).example('xxx.xxx@xxx.xxx').description('email'),
	password: Joi.string().example('I\'mAP@ssw0rd').description('password')
};

module.exports = {
	rows: rows
};
