'use strict';

const Joi = require('@hapi/joi');
const SchemaCompilation = require('../utils/SchemaCompilation.js');

const helpModel = require('./helpModel');

const structure = {
	uuid: {schema: helpModel.rows.uuid, categories: ['!get', 'insert'], sql: {function: {get: 'UuidFromBin', insert: 'UuidToBin'}}},
	name: {schema: Joi.string(), categories: ['!get', '!put', '!insert']},
	parentUuid: {schema: helpModel.rows.uuid, categories: ['get', 'put', 'insert'], sql: {function: {get: 'UuidFromBin'}, join: {column: 'uuid', table: 'departments', alias: 'parent', condition: 'departments.parent_id = parent.id'}}},
	parentName: {schema: Joi.string(), categories: ['get'], sql: {join: {column: 'name', table: 'departments', alias: 'parent', condition: 'departments.parent_id = parent.id'}}},
	address: {schema: Joi.string(), categories: ['get', 'put', 'insert']},
	level: {schema: Joi.number(), categories: ['get'], sql: false},
	links: {schema: helpModel.rows.links, categories: ['!get'], sql: false}
};

const schemas = SchemaCompilation.toSchemas(structure, {get: 'Get Department Object', put: 'Put Department Object'});
const sqlQueries = {
	select: SchemaCompilation.toSqlQuerie(structure, 'select', 'get', 'departments'),
	insert: SchemaCompilation.toSqlQuerie(structure, 'insert', 'insert', 'departments'),
	selectRecursive : 'WITH RECURSIVE ancestors(id, uuid, name, address, level, parent_uuid, path) AS ( ' +
		'SELECT id, uuid, name, address, 0, CAST(NULL AS CHAR(255)), CAST(name AS CHAR(255)) FROM `departments` WHERE parent_id IS NULL ' +
		'UNION ' +
		'SELECT d.id, d.uuid, d.name, d.address, a.level+1, a.uuid, CONCAT(a.path, \'/\', d.name) FROM `departments` AS d, `ancestors` AS a WHERE d.parent_id = a.id ' +
	') ' +
	'SELECT UuidFromBin(uuid) as uuid, name, address, level, UuidFromBin(parent_uuid) as parent_uuid FROM ancestors ORDER BY path'
};

const schemaFromDao = (dao, linker) => {
	return {
		uuid: '' + dao.uuid,
		name: dao.name,
		parentUuid: dao.parent_uuid? '' + dao.parent_uuid : undefined,
		parentName: dao.parent_name || undefined,
		address: dao.address|| undefined,
		level: Number.isInteger(dao.level)? dao.level : undefined,
		links: linker.makeLinks({
			SELF: ['GET', '/departments/{departmentUuid}', {params: {departmentUuid: dao.uuid}}],
			PARENT: ['GET', '/departments/{departmentUuid}', {params: {departmentUuid: dao.parent_uuid}}]
		})
	};
};

module.exports = {
	schemas: schemas,
	sqlQueries: sqlQueries,
	schemaFromDao: schemaFromDao,
	schemasFromDaos: function(dao, linker) {
    return dao.reduce((acc, val) => {
      acc.push(schemaFromDao(val, linker));
	   	return acc;
    }, []);
	}
};
