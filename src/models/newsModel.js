'use strict';

const Joi = require('@hapi/joi');
const SchemaCompilation = require('../utils/SchemaCompilation.js');

const helpModel = require('./helpModel');

const structure = {
	uuid: {schema: helpModel.rows.uuid, categories: ['!get', 'insert'], sql: {function: {get: 'UuidFromBin', insert: 'UuidToBin'}}},
	creationDate: {schema: helpModel.rows.timestamp, categories: ['!get']},
	creatorUuid: {schema: helpModel.rows.uuid, categories: ['!get', '!insert'], sql: {function: {get: 'UuidFromBin', insert: 'UuidFromBin'}, join: {column: 'uuid', table: 'staff', alias: 'creator', condition: 'news.creator_id = creator.id'}}},
	creatorName: {schema: Joi.string(), categories: ['!get'], sql: {join: {column: 'name', table: 'staff', alias: 'creator', condition: 'news.creator_id = creator.id'}}},
	content: {schema: Joi.string(), categories: ['!get', '!put', '!insert']},
	departmentUuid: {schema: helpModel.rows.uuid, categories: ['!get', '!put', '!insert'], sql: {function: {get: 'UuidFromBin', insert: 'UuidToBin'}, join: {column: 'uuid', table: 'departments', alias: 'department', condition: 'news.department_id = department.id'}}},
	departmentName: {schema: Joi.string(), categories: ['!get', '!put', '!insert'], sql: {join: {column: 'name', table: 'departments', alias: 'department', condition: 'news.department_id = department.id'}}},
	links: {schema: helpModel.rows.links, categories: ['!get'], sql: false}
};

const schemas = SchemaCompilation.toSchemas(structure, {get: 'Get News Object', put: 'Put Department Object'});
const sqlQueries = {
	select: SchemaCompilation.toSqlQuerie(structure, 'select', 'get', 'news'),
	insert: SchemaCompilation.toSqlQuerie(structure, 'insert', 'insert', 'news')
};

const schemaFromDao = (dao, linker) => {
	return {
		uuid: '' + dao.uuid,
		creationDate: Math.round(new Date(dao.creation_date).getTime()/1000),
		creatorUuid: '' + dao.creator_uuid,
		creatorName: dao.creator_name,
		content: dao.content,
		departmentUuid: '' + dao.department_uuid,
		departmentName: dao.department_name,
		links: linker.makeLinks({
			SELF: ['GET', '/news/{newsUuid}', {params: {newsUuid: dao.uuid}}],
			CREATOR: ['GET', '/staff/{staffUuid}', {params: {staffUuid: dao.creator_uuid}}],
			DEPARTMENT: ['GET', '/departments/{departmentUuid}', {params: {departmentUuid: dao.department_uuid}}]
		})
	};
};

module.exports = {
	schemas: schemas,
	sqlQueries: sqlQueries,
	schemaFromDao: schemaFromDao,
	schemasFromDaos: function(dao, linker) {
    return dao.reduce((acc, val) => {
      acc.push(schemaFromDao(val, linker));
	   	return acc;
    }, []);
	}
};
