'use strict';

const logger = require('./logger')('ConfigManager');
const fs = require('fs');
const semver = require('semver');
const prompts = require('prompts');
const mysql = require('mysql2/promise');
const mysqlPassword = require('mysql-password');
const passwordGenerator = require('generate-password');

const { version } = require('../../package.json');
const config = require('./config');

const rootDbAction = (() => {
  let rootConnection;
  const execute = async (queries, params) => {
    params = params || [];
    if(!rootConnection) {
      logger.debug('To configure database, please enter credentials of an administrator account (these will not be saved)');
      const { username, password } = await prompts([
        { type: 'text', name: 'username', message: 'Username', initial: 'root' },
        { type: 'invisible', name: 'password', message: 'Password' }
      ], { onCancel: () => true });
      if(!username || !password) logger.exit('Aborted!');

      rootConnection = await mysql.createConnection({
        host: config.get('db_host'),
        port: config.get('db_port'),
        database: config.get('db_name'),
        user: username,
        password: password,
        multipleStatements: true
      });
    }
    await rootConnection.query(queries, params)
      .catch((error) => { throw(error); });
  };

  const executeFile = async (path) => {
    const fileContent = fs.readFileSync(path, 'utf-8');
    if(!fileContent) {
      logger.fatal('no file content', fileContent);
      throw new Error();
    }

    await execute(fileContent);
  };

  return {
    executeFile: executeFile,
    execute: execute,
    close: () => rootConnection? rootConnection.close() : null
  };
})();

const plateformConfig = async () => {
  const fromEnv = true;

  if(!config.contains('plateform_name', {fromEnv}) || !config.contains('api_port', {fromEnv}) || !config.contains('api_host', {fromEnv}) || !config.contains('api_url', {fromEnv})) {
    logger.debug('Please, configure the plateform', {NODE_ENV: config.NODE_ENV});
    if(!await config.ask('plateform_name', 'text', {message: 'Enter the public plateform name'})
      || !await config.ask('api_port', 'number', {message: 'Enter the internal api port'})
      || !await config.ask('api_host', 'text', {message: 'Enter the internal api interface'})
      || !await config.ask('api_url', 'text', {message: 'Enter the public api url'})) {
      logger.exit('Configuration aborted! No name, port, host or url', config.get('plateform_name', {fromEnv}), config.get('api_port', {fromEnv}), config.get('api_host', {fromEnv}), config.get('api_url', {fromEnv}));
    }
  }

  if(!config.contains('jwt_keypath', {fromEnv})) {
    if(!await config.ask('jwt_keypath', 'text', {message: 'Please, set the key file path for jwt', defaultValue: './'+config.get('NODE_ENV')+'Private.pem'})) {
      logger.exit('Configuration aborted! No key file path', config.get('jwt_keypath'));
    }
  }
};

const dbConnectionConfig = async (reset) => {
  const fromEnv = true;

  if(reset || !config.contains('db_port', {fromEnv}) || !config.contains('db_host', {fromEnv}) || !config.contains('db_name', {fromEnv})) {
    logger.debug('Please, create the database table and enter network informations...', {NODE_ENV: config.NODE_ENV});
    if(!await config.ask('db_port', 'number', {message: 'Enter the database port'})
      || !await config.ask('db_host', 'text', {message: 'Enter the database interface'})
      || !await config.ask('db_name', 'text', {message: 'Enter the database name'})) {
      logger.exit('Configuration aborted! No port, host or database name', config.get('db_port', {fromEnv}), config.get('db_host', {fromEnv}), config.get('db_name', {fromEnv}));
    }
  }

  let configureDatabaseUser;
  if(reset || !config.contains('db_username', {fromEnv: true}) || !config.contains('db_password', {fromEnv: true})) {
    logger.debug('Database credentials for ' + config.get('db_name') + ' are not set. For security reasons, we recommend using a specific user for the table (no root, no grant option...).');
    ({ configureDatabaseUser } = await prompts({ name: 'configureDatabaseUser', type: 'confirm', message: 'Would you like openCaserne\'s assistance to create the user (only if not already created)?', initial: true }));

    if(!await config.ask('db_username', 'text', {defaultValue: config.get('db_username') || config.get('db_name'), message: 'Enter the database table specific user name'})) {
      logger.exit('Configuration aborted! No database username', config.get('db_username'));
    }
    if(!await config.ask('db_password', 'invisible', {defaultValue: false, message: 'Enter the database specific user password' + (configureDatabaseUser? ' (empty to generate new password)' : '')})) {
      config.set('db_password', passwordGenerator.generate({length: 20, numbers: true, symbols: true}));
    }

    if(configureDatabaseUser) {
      const { createDatabaseUser } = await prompts({ name: 'createDatabaseUser', type: 'confirm', message: 'openCaserne can create user itself with database root/grant credentials (not saved). If you don\'t want, we will display you queries to create user.', initial: true });

      const databaseUser = '\'' + config.get('db_username') + '\'@\'' + config.get('db_host') + '\'';
      const createUserQueries = 'CREATE USER ' + databaseUser + ' IDENTIFIED BY PASSWORD \'' + mysqlPassword(config.get('db_password')) + '\';\nGRANT DELETE, INSERT, SELECT, UPDATE, EXECUTE ON `' + config.get('db_name') + '`.* TO ' + databaseUser + ';';

      if(createDatabaseUser) await rootDbAction.execute(createUserQueries);
      else {
        logger.debug('To create user, you will execute these commands:\n', createUserQueries);
        await prompts({ type: 'confirm', message: 'Queries were executed without errors', initial: false });
      }
    }
  }

  await mysql.createConnection({
    host: config.get('db_host'),
    port: config.get('db_port'),
    database: config.get('db_name'),
    user: config.get('db_username'),
    password: config.get('db_password')
  })
  .then((connection) => connection.close())
  .catch(async (error) => {
    logger.error(error);
    const { action } = await prompts({ name: 'action', type: 'select', message: 'BDD connection fail. Do you yant set settings again, retry or abort connection?', choices: [
      { title: 'Retry', value: 'retry' },
      { title: 'Setting again', value: 'again' },
      { title: 'Abort', value: 'abort' }
    ]});

    console.log(action);
    switch(action) {
      case 'again':
        await dbConnectionConfig(true);
        break;
      case 'retry':
        await dbConnectionConfig(false);
        break;
      default:
        process.exit(1);
    }
    return;
  })
};

const dbVersionConfig = async () => {
  const connection = await mysql.createConnection({
    host: config.get('db_host'),
    port: config.get('db_port'),
    database: config.get('db_name'),
    user: config.get('db_username'),
    password: config.get('db_password')
  });
  let dbVersion;
  await connection.query('SELECT `version` FROM `version_history` WHERE `isCompleted` = 1 ORDER BY id DESC LIMIT 1')
  .then(([results, fields]) => {
    if(results[0] && results[0].version) {
      dbVersion = results[0].version;
      logger.debug('dbVersion', dbVersion);
    }
    else logger.fatal('No version_history data but table exist...');
  })
  .catch((error) => {
    if(error && error.code !== 'ER_NO_SUCH_TABLE') {
      logger.fatal(error);
    }
    dbVersion = '0.0.0';
  });
  connection.close();

  const configs = [
    { dbVersion: '0.0.1', dbScript: './sqlUpdate/version-0.0.1.sql' }
  ];

  let newDbVersion = dbVersion;
  for(let i in configs) {
    const config = configs[i];
    if(semver.lt(dbVersion, config.dbVersion)) {
      try {
        await rootDbAction.executeFile(config.dbScript);
        await rootDbAction.execute('INSERT INTO `version_history` (`version`, `isCompleted`) VALUES (?, 1)', [config.dbVersion]);
        logger.debug('Database updated to', config.dbVersion);
      } catch(error) {
        logger.error(error);
        await rootDbAction.execute('INSERT INTO `version_history` (`version`, `error`) VALUES (?, ?)', [config.dbVersion, error.toString()]);
        logger.fatal('close');
      }
    }
  }

};

const check = async() => {
  await plateformConfig();

  await dbConnectionConfig();
  await config.save();

  await dbVersionConfig();
  await config.save();

  rootDbAction.close();
};

module.exports = { check };
