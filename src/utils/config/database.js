'use strict';

const logger = require('../logger')('Database');
const mysql = require('mysql2');
const config = require('../config');

const database = () => {
		const dbConnection = mysql.createConnection({
	    host: config.get('db_host'),
	    port: config.get('db_port'),
	    database: config.get('db_name'),
	    user: config.get('db_username'),
	    password: config.get('db_password')
		});

		dbConnection.on('error', function(error) {
			logger.debug('DB Error', error);
		});

		return dbConnection;
};

module.exports = database;

/*
Récupérer les groupes triés et avec hierarchie

WITH RECURSIVE ancestors(id, uuid, name, address, level, parent_uuid, path) AS (
    SELECT id, uuid, name, address, 0, CAST(NULL AS BINARY(16)), CAST(name AS CHAR(255)) FROM `departments` WHERE parent_id IS NULL
    UNION
    SELECT d.id, d.uuid, d.name, d.address, a.level+1, a.uuid, CONCAT(a.path, '/', d.name) FROM `departments` AS d, `ancestors` AS a WHERE d.parent_id = a.id
)
SELECT UuidFromBin(uuid) as uuid, name, address, level, UuidFromBin(parent_uuid) as parent_uuid FROM ancestors ORDER BY path;




Récupérer les compétences triés et avec hierarchie

WITH RECURSIVE ancestors(id, uuid, name, short_name, description, level, parent_uuid, path) AS (
    SELECT id, uuid, name, short_name, description, 0, CAST(NULL AS CHAR(255)), CAST(name AS CHAR(255)) FROM `skills` WHERE parent_id IS NULL
    UNION
    SELECT d.id, d.uuid, d.name, d.short_name, d.description, a.level+1, a.uuid, CONCAT(a.path, '/', d.name) FROM `skills` AS d, `ancestors` AS a WHERE d.parent_id = a.id
)
SELECT UuidFromBin(uuid) as uuid, name, short_name, description, level, UuidFromBin(parent_uuid) as parent_uuid FROM ancestors ORDER BY path;



SELECT events.name,
(
  SELECT CONCAT('[', GROUP_CONCAT(CONCAT('{"skill":{"uuid":"', UuidFromBin(s.uuid), '", "name":"', s.name, '", "shortName":"', s.short_name, '"', IFNULL(CONCAT(', "description":"', s.description, '"'), ''), '}, "requiredQuantity":', required_quantity, '}') SEPARATOR ', '), ']') skills
  FROM events_skills
  JOIN skills s ON s.id = skill_id
  WHERE event_id = events.id
) AS skills,
(
  SELECT CONCAT('[', GROUP_CONCAT(CONCAT('{"uuid":"', UuidFromBin(staff.uuid), '", "username":"', staff.username, '", "name":"', staff.name, '", "surname":"', staff.surname, '"', IFNULL(CONCAT(', "skill":"', UuidFromBin(s.uuid), '"'), ''), ', "skills":[',
    IFNULL((
      SELECT GROUP_CONCAT(CONCAT('{"uuid":"', UuidFromBin(s.uuid), '", "name":"', s.name, '", "shortName":"', s.short_name, '"', IFNULL(CONCAT(', "description":"', s.description, '"'), ''), '}') SEPARATOR ', ')
      FROM staff_skills
      JOIN skills s ON s.id = skill_id
      WHERE staff_skills.staff_id = events_staff.staff_id
    ), '')
  , ']}') SEPARATOR ', '), ']')
  FROM events_staff
  JOIN staff ON staff.id = staff_id
  LEFT JOIN skills s ON s.id = events_staff.skill_id
  WHERE event_id = events.id
) AS staffSkills
FROM events


WITH name_tree AS (
    SELECT id, uuid, name, parent_id FROM `departments` WHERE id = 20
    UNION
    SELECT d.id, d.uuid, d.name, d.parent_id, a.uuid FROM `departments` AS d JOIN `departments` AS a WHERE a.parent_id = d.id
)
SELECT UuidFromBin(uuid), name, parent_id, * FROM name_tree;
*/
