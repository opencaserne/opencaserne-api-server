'use strict';

const logger = require('../logger')('Config');
const fs = require('fs');
const path = require('path');
const prompts = require('prompts');

const loadOrCreateEmptyConfiguration = (fileName) => {
  const filePath = path.join(__dirname, fileName);
  if(fs.existsSync(filePath)) {
    logger.debug('Load', fileName, '...');
    const data = fs.readFileSync(filePath, 'utf-8');
    const config = JSON.parse(data);
    logger.success('Loaded', fileName);
    return config;
  }

  return {};
};

const saveConfigurationFile = (fileName, config) => {
  const filePath = path.join(__dirname, fileName);
  logger.debug('Save', fileName, '...');
  const data = JSON.stringify(config, null, 2);
  fs.writeFileSync(filePath, data);
  logger.success('Saved', fileName);
};

logger.debug('ENV', JSON.stringify(process.env));
const defaultConfigFileName = 'default.json';
let defaultConfig = loadOrCreateEmptyConfiguration(defaultConfigFileName);

const NODE_ENV = process.env.NODE_ENV || 'production';
const environmentConfigFileName = NODE_ENV.toLowerCase() + 'Config.json';
let environmentConfig = loadOrCreateEmptyConfiguration(environmentConfigFileName);

let needToBeSaved = false;
process.once('beforeExit', (code) => {
  if(needToBeSaved) {
    saveConfigurationFile(environmentConfigFileName, environmentConfig);
  }
  else {
    logger.debug('Configuration don\'t need to be saved...', {code});
  }
});

const set = (name, value, options) => {
  needToBeSaved = true;
  return (options || {}).toDefault? defaultConfig[name] = value : environmentConfig[name] = value;
};

const ask = async (name, type, options) => {
  options = options || {};

  const message = options.message || 'Please, type a value for configuration ' + name;
  const defaultValue = options.defaultValue !== false? options.defaultValue || get(name, options) || null : null;
  const { result } = await prompts({ name: 'result', type, message, initial: defaultValue });

  if(result) set(name, result, options);
  return result;
};

const get = (name, options) => {
  options = options || {};

  if(name === 'NODE_ENV') return NODE_ENV;

  let defaultFailback = false;
  if(process.env.OC_USE_DEFAULT_CONFIG_FAILBACK === "all" || process.env.OC_USE_DEFAULT_CONFIG_FAILBACK.split(',').indexOf(name) >= 0) {
    defaultFailback = true;
  }

  if(options.fromEnv && !defaultFailback) return process.env['OC_' + name.toUpperCase()] || environmentConfig[name];
  else if(options.fromDefault) return defaultConfig[name];
  return process.env['OC_' + name.toUpperCase()] || environmentConfig[name] || defaultConfig[name];
};

const contains = (name, options) => {
  return 'undefined' !== typeof get(name, options);
};

const remove = (name, options) => {
  needToBeSaved = true;
  (options || {}).fromDefault? delete defaultConfig[name] : delete environmentConfig[name];
};

const save = () => {
  if(needToBeSaved) {
    saveConfigurationFile(environmentConfigFileName, environmentConfig);
  }
};

const config = { set, ask, contains, get, remove, save };

module.exports = config;
