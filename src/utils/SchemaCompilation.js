'use strict';

const Joi = require('@hapi/joi');

const structureToSchemas = (structure, definitions) => {
  const compilation = {RAW: {}};

  for(let modelName in structure) {
    const model = structure[modelName];

    model.categories.forEach((categorieName) => {

      const isRequired = categorieName.startsWith('!');
      if(isRequired) {
        categorieName = categorieName.substring(1);
      }

      if(!compilation[categorieName]) {
        compilation[categorieName] = {};
      }

      compilation.RAW[modelName] = model.schema;

      try {
        compilation[categorieName][modelName] = isRequired? model.schema.required() : model.schema;
      } catch(e) {
        console.error('e', modelName, e);
      }
    });
  }

  for(let categorie in compilation) {
    if(categorie !== 'RAW') {
      compilation[categorie] = Joi.object(compilation[categorie]);

      if(definitions && definitions[categorie]) {
        compilation[categorie] = compilation[categorie].label(definitions[categorie])
      }
    }
  }

  return compilation;
};

const camelToSnake = (string) => string.replace(/[\w]([A-Z])/g, (m)  => m[0] + "_" + m[1]).toLowerCase();
const snakeToCamel = (string) => string.replace(/(_\w)/g, (m) => m[1].toUpperCase());

const structureToSqlQuerie = (structure, type, categorie, table) => {
	const modelList = [];
  const joinList = [];

	for(let modelName in structure) {
    const model = structure[modelName];

		if((model.categories.indexOf(categorie) >= 0 || model.categories.indexOf('!' + categorie) >= 0) && model.sql !== false) {
			const sqlConf = model.sql || {};
      const joinConf = sqlConf.join || {};

      const resultColumnName = camelToSnake(modelName);

      let obj;
      if(sqlConf.subquery) {
        obj = '(' + sqlConf.subquery + ') as ' + resultColumnName;
      }
      else {
        const dataColumnName = joinConf.column || sqlConf.column || resultColumnName;
        const dataTable = joinConf.table || table;
        const dataTableAlias = joinConf.alias || dataTable;

        if(sqlConf.join) {
          const tmp = (joinConf.type? joinConf.type.toUpperCase() : 'LEFT') + ' JOIN ' + dataTable + (dataTable !== dataTableAlias? ' as ' + dataTableAlias : '') + ' ON ' + joinConf.condition;
          if(joinList.indexOf(tmp) === -1) joinList.push(tmp)
        }

  			const nameTable = dataTableAlias + '.' + dataColumnName;

  			if(type === 'select') {
  				const func = sqlConf.function? sqlConf.function[categorie] || sqlConf.function['default'] || (typeof sqlConf.function === 'string'? sqlConf.function : false) : false;
          if(nameTable === 'manager.uuid') {
            console.log('func', func, sqlConf.function);
          }
  				obj = func? func + '(' + nameTable + ') as ' + resultColumnName : nameTable + (dataColumnName !== resultColumnName? ' as ' + resultColumnName : '');
  			}
  			else {
  				obj = dataColumnName;
  			}
      }

			modelList.push(obj);
		}
	}

	let operation = type.toUpperCase() + ' ';
	if(type === 'insert') {
		operation += 'INTO ' + table + ' (';
	}

	operation += modelList.join(', ');

	if(type === 'insert') {
			operation += ') VALUES ';
	} else if(type === 'select') {
		operation += ' FROM ' + table + ' ' + joinList.join(' ');
	}

	return operation;
};

module.exports = {
  toSchemas: structureToSchemas,
  toSqlQuerie: structureToSqlQuerie
}
