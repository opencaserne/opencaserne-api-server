'use strict';

const getRouteInfo = function(request, method, uri, data) {

	const routeInfo = {
		hasAccess: undefined,
		isAuthRoute: undefined,
		auth: undefined
	};

	data = data || {};

	const fakeRequest = {
		auth: {
			credentials: request.auth.credentials
		},
		params: data.params,
		query: data.query,
		_log: () => {}
	};
	const route = request.server.match(method, uri);
	if(route) {
		routeInfo.isAuthRoute = route.settings.tags.indexOf('auth') > -1;
		routeInfo.auth = route.settings.auth && route.settings.auth.mode === 'required'? 'session' : false;
	}

	try {
		routeInfo.hasAccess = request._core.auth._access(fakeRequest, route);
	}
	catch (err) {
		routeInfo.hasAccess = false;
	}

	return routeInfo;
};

module.exports = function(request) {

	return {
		hasAccess: (method, uri, data) => {
			const routeInfo = getRouteInfo(request, method, uri, data);
			return routeInfo.hasAccess;
		},
		makeLinks(routes) {

			const links = {};

			for(const name in routes) {

				const route = routes[name];
				const method = route[0];
				let uri = route[1];
				const data = route[2] || {};

				const routeInfo = getRouteInfo(request, method, uri, data);
				if(routeInfo.hasAccess) {

					if(data.params) {
						for(const paramName in data.params) {
							uri = uri.replace('{'+paramName+'}', encodeURIComponent(data.params[paramName]));
						}
					}
					if(data.query) {
						let queries = '';
						for(const queryName in data.query) {
							queries += (queries? '&' : '?' ) + encodeURIComponent(queryName) + '=' + encodeURIComponent(data.query[queryName]);
						}
						uri += queries;
					}
					links[name] = {
						isAuthRoute: routeInfo.isAuthRoute? true : undefined,
						method: method,
						href: uri,
						auth: routeInfo.auth? routeInfo.auth : undefined
					};
				}
			}

			return links;
		}
	};
};
