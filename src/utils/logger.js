'use strict';

const styles = {
	reset: '\x1b[0m',

	resetcolor: '\x1b[39m',
	red: '\x1b[31m',
	green: '\x1b[32m',
	yellow: '\x1b[33m',
	blue: '\x1b[34m',
	magenta: '\x1b[35m',
	cyan: '\x1b[36m',
	lred: '\x1b[91m',
	lgreen: '\x1b[92m',
	lyellow: '\x1b[93m',
	lblue: '\x1b[94m',
	lmagenta: '\x1b[95m',
	lcyan: '\x1b[96m',

	resetbgcolor: '\x1b[49m',
	bgred: '\x1b[41m',
	bggreen: '\x1b[42m',
	bgyellow: '\x1b[43m',
	bgblue: '\x1b[44m',
	bgmagenta: '\x1b[45m',
	bgcyan: '\x1b[46m',
	bglred: '\x1b[101m',
	bglgreen: '\x1b[102m',
	bglyellow: '\x1b[103m',
	bglblue: '\x1b[104m',
	bglmagenta: '\x1b[105m',
	bglcyan: '\x1b[106m',

	underline: '\x1b[4m',
	resetunderline: '\x1b[24m',
	blink: '\x1b[5m',
	resetblink: '\x1b[25m'
};
const tagColors = [styles.lred, styles.green, styles.lyellow, styles.lblue, styles.lmagenta, styles.lcyan];

const log = function(tag, style, args) {
	args = Array.prototype.slice.call(args);
	args.splice(0, 0, styles.reset + '[' + styles.yellow + new Date().toLocaleString() + styles.reset + '][' + styles.lred + tag + styles.reset + ']' + style);
	args.push(styles.reset);

	console.log.apply(this, args);
};

let tagColorI = 0;
const logger = (tag) => {
	const tagColor = tagColors[tagColorI++%tagColors.length];

	function debug() {
		log(tagColor + tag, styles.resetcolor, arguments);
	}

	function warning() {
		log(tagColor + tag, styles.yellow, arguments);
	}

	function success() {
		log(tagColor + tag, styles.lgreen, arguments);
	}

	function error() {
		log(tagColor + tag, styles.red, arguments);
	}

	function fatal() {
		log(tagColor + tag, styles.bgred + styles.yellow + 'FATAL:' + styles.resetbgcolor, arguments);
		process.exit(1);
	}

	function exit() {
		log(tagColor + tag, styles.bgred + 'EXIT:' + styles.resetbgcolor, arguments);
		process.exit(0);
	}

	return {
		debug,
		warning,
		success,
		fatal,
		error,
		exit
	};
};

module.exports = logger;
