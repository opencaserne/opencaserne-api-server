'use strict';

const logger = require('../utils/logger')(module.filename);
const linker = require('../utils/linker');
const database = require('../utils/config/database');
const Boom = require('@hapi/boom');
const bcrypt = require('bcrypt');

const authModel = require('../models/authModel');
const staffModel = require('../models/staffModel');

module.exports = {
	logIn: (request, h) => new Promise((resolve, reject) => {

		const query = staffModel.sqlQueries.authSelect + ' WHERE staff.email = ? OR staff.username = ? LIMIT 1';
		const params = [request.payload.emailOrUsername, request.payload.emailOrUsername];
		let connection = database();
		connection.execute(query, params, (error, results, fields) => {
			connection.close();
			if(error) {
				logger.error(error);
				return reject(Boom.serverUnavailable('Database error'));
			}

			if(results.length <= 0 || !bcrypt.compareSync(request.payload.password, results[0].password_hash)) {
				return reject(Boom.badRequest('Connection failed: bad username or password'));
			}

			const result = results[0];
			const scope = ['type-user', 'user-'+result.uuid, 'department-'+result.department_uuid];

			const fakeRequest = {
				auth: {credentials: {scope: scope}},
				server: request.server,
				_core: request._core
			};
			const staff = staffModel.schemaFromDao(result, linker(fakeRequest));

			const apiKey = request.server.methods.jwtsign({
				staffUuid: staff.uuid,
				scope: scope
			});

			const auth = authModel.schemaFromTokenAndUser(apiKey, staff, linker(fakeRequest));
			resolve(auth);
		});
	}),
	logOut: (request, h) => Boom.notImplemented('method not implemented logOut'),

	passwordReset: (request, h) => Boom.notImplemented('method not implemented logOut'),
	passwordUpdate: (request, h) => Boom.notImplemented('method not implemented logOut')
};
