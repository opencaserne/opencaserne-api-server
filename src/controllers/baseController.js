 'use strict';

const logger = require('../utils/logger')(module.filename);
const linker = require('../utils/linker');
const database = require('../utils/config/database');

module.exports = {
	list: (request, h) => linker(request).makeLinks({
	  LOG_IN: ['POST', '/auth/session'],
		PASSWORD_RESET: ['POST', '/auth/password/reset'],
	  PASSWORD_UPDATE: ['POST', '/auth/password/update']
	})
};
