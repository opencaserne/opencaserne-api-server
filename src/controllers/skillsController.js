'use strict';

const logger = require('../utils/logger')(module.filename);
const linker = require('../utils/linker');
const database = require('../utils/config/database');
const Boom = require('@hapi/boom');

const skillsModel = require('../models/skillsModel');

module.exports = {

	find: (request, h) => new Promise((resolve, reject) => {
		let query = skillsModel.sqlQueries.selectRecursive;

		let connection = database();
		connection.execute(query, (error, results, fields) => {
			connection.close();
			if(error) {
				logger.error(error);
				return reject(Boom.serverUnavailable('Database error'));
			}

			const skills = skillsModel.schemasFromDaos(results, linker(request));
			resolve(skills);
		});
	}),

	findByskillUuid: (request, h) => new Promise((resolve, reject) => {
		const query = skillsModel.sqlQueries.select + ' WHERE skills.uuid = UuidToBin(?) LIMIT 1';
		const params = [request.params.skillUuid];
		let connection = database();
		connection.execute(query, params, (error, results, fields) => {
			connection.close();
			if(error) {
				logger.error(error);
				return reject(Boom.serverUnavailable('Database error'));
			}

			if(results.length <= 0) {
				return reject(Boom.notFound('Ressource not found'));
			}

			const skill = skillsModel.schemaFromDao(results[0], linker(request));
			resolve(skill);
		});
	})
};
