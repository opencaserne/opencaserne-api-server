'use strict';

const logger = require('../utils/logger')(module.filename);
const linker = require('../utils/linker');
const database = require('../utils/config/database');
const Boom = require('@hapi/boom');

const newsModel = require('../models/newsModel');

module.exports = {

	find: (request, h) => new Promise((resolve, reject) => {
		let query = newsModel.sqlQueries.select + ' ORDER BY creation_date DESC';
		let connection = database();
		connection.execute(query, (error, results, fields) => {
			connection.close();
			if(error) {
				logger.error(error);
				return reject(Boom.serverUnavailable('Database error'));
			}

			const news = newsModel.schemasFromDaos(results, linker(request));
			resolve(news);
		});
	}),

	findByNewsUuid: (request, h) => new Promise((resolve, reject) => {
		const query = newsModel.sqlQueries.select + ' WHERE news.uuid = UuidToBin(?) LIMIT 1';
		const params = [request.params.newsUuid];
		let connection = database();
		connection.execute(query, params, (error, results, fields) => {
			connection.close();
			if(error) {
				logger.error(error);
				return reject(Boom.serverUnavailable('Database error'));
			}

			if(results.length <= 0) {
				return reject(Boom.notFound('Ressource not found'));
			}

			const news = newsModel.schemaFromDao(results[0], linker(request));
			resolve(news);
		});
	})
};
