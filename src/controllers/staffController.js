'use strict';

const logger = require('../utils/logger')(module.filename);
const linker = require('../utils/linker');
const database = require('../utils/config/database');
const Boom = require('@hapi/boom');

const staffModel = require('../models/staffModel');

module.exports = {

	find: (request, h) => new Promise((resolve, reject) => {
		let query = staffModel.sqlQueries.select;
		let connection = database();
		connection.execute(query, (error, results, fields) => {
			connection.close();
			if(error) {
				logger.error(error);
				return reject(Boom.serverUnavailable('Database error'));
			}

			const staff = staffModel.schemasFromDaos(results, linker(request));
			resolve(staff);
		});
	}),

	findByStaffUuid: (request, h) => new Promise((resolve, reject) => {
		const query = staffModel.sqlQueries.select + ' WHERE staff.uuid = UuidToBin(?) LIMIT 1';
		const params = [request.params.staffUuid];
		let connection = database();
		connection.execute(query, params, (error, results, fields) => {
			connection.close();
			if(error) {
				logger.error(error);
				return reject(Boom.serverUnavailable('Database error'));
			}

			if(results.length <= 0) {
				return reject(Boom.notFound('Ressource not found'));
			}

			const staff = staffModel.schemaFromDao(results[0], linker(request));
			resolve(staff);
		});
	})
};
