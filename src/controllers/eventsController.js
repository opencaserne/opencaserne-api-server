'use strict';

const logger = require('../utils/logger')(module.filename);
const linker = require('../utils/linker');
const database = require('../utils/config/database');
const Boom = require('@hapi/boom');

const eventsModel = require('../models/eventsModel');

module.exports = {

	find: (request, h) => new Promise((resolve, reject) => {
		let query = eventsModel.sqlQueries.select + ' WHERE events.end_date > NOW() ORDER BY events.begin_date ASC';
		let connection = database();
		connection.execute(query, (error, results, fields) => {
			connection.close();
			if(error) {
				logger.error(error);
				return reject(Boom.serverUnavailable('Database error'));
			}

			const events = eventsModel.schemasFromDaos(results, linker(request));
			resolve(events);
		});
	}),

	findByEventUuid: (request, h) => new Promise((resolve, reject) => {
		const query = eventsModel.sqlQueries.select + ' WHERE events.uuid = UuidToBin(?) LIMIT 1';
		const params = [request.params.eventUuid];
		let connection = database();
		connection.execute(query, params, (error, results, fields) => {
			connection.close();
			if(error) {
				logger.error(error);
				return reject(Boom.serverUnavailable('Database error'));
			}

			if(results.length <= 0) {
				return reject(Boom.notFound('Ressource not found'));
			}

			const event = eventsModel.schemaFromDao(results[0], linker(request));
			resolve(event);
		});
	})
};
