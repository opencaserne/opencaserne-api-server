'use strict';

const logger = require('../utils/logger')(module.filename);
const linker = require('../utils/linker');
const database = require('../utils/config/database');
const Boom = require('@hapi/boom');

const departmentsModel = require('../models/departmentsModel');

module.exports = {

	find: (request, h) => new Promise((resolve, reject) => {
		let query = departmentsModel.sqlQueries.selectRecursive;

		let connection = database();
		connection.execute(query, (error, results, fields) => {
			connection.close();
			if(error) {
				logger.error(error);
				return reject(Boom.serverUnavailable('Database error'));
			}

			const departments = departmentsModel.schemasFromDaos(results, linker(request));
			resolve(departments);
		});
	}),

	findByDepartmentUuid: (request, h) => new Promise((resolve, reject) => {
		const query = departmentsModel.sqlQueries.select + ' WHERE departments.uuid = UuidToBin(?) LIMIT 1';
		const params = [request.params.departmentUuid];
		let connection = database();
		connection.execute(query, params, (error, results, fields) => {
			connection.close();
			if(error) {
				logger.error(error);
				return reject(Boom.serverUnavailable('Database error'));
			}

			if(results.length <= 0) {
				return reject(Boom.notFound('Ressource not found'));
			}

			const department = departmentsModel.schemaFromDao(results[0], linker(request));
			resolve(department);
		});
	})
};
